import { permission } from '@/utils/permission'

export default {
	mounted(el, binding) {
		const { value } = binding
		if(Array.isArray(value)){
			let ishas = false;
			value.forEach(item => {
				if(permission(item)){
					ishas = true;
				}
			})
			if (!ishas){
				console.log(value + ' 权限不足')
				el.parentNode.removeChild(el)
			}
		}else{
			if(!permission(value)){
				console.log(value + ' 权限不足')
				el.parentNode.removeChild(el);
			}
		}
	}
};
