import API from "@/api";

//文件选择器配置

export default {
	apiObj: API.common.upload,
	menuApiObj: API.gdshop.attachmentGroup,
	listApiObj: API.gdshop.attachment,
	successCode: 0,
	maxSize: 30,
	max: 99,
	uploadParseData: function (res) {
		return {
			id: res.data.id,
			fileName: res.data.original_name,
			url: res.data.url,
			baseUrl: res.data.base_url,
		}
	},
	listParseData: function (res) {
		return {
			rows: res.data.list,
			total: res.data?.pagination?.total,
			msg: res.message,
			code: res.code
		}
	},
	request: {
		page: 'page',
		pageSize: 'pageSize',
		keyword: 'keyWord',
		menuKey: 'group_id'
	},
	menuProps: {
		key: 'id',
		label: 'group_name',
		children: 'children'
	},
	fileProps: {
		key: 'id',
		fileName: 'original_name',
		url: 'url',
		baseUrl: 'base_url',
	},
	files: {
		doc: {
			icon: 'sc-icon-file-word-2-fill',
			color: '#409eff'
		},
		docx: {
			icon: 'sc-icon-file-word-2-fill',
			color: '#409eff'
		},
		xls: {
			icon: 'sc-icon-file-excel-2-fill',
			color: '#67C23A'
		},
		xlsx: {
			icon: 'sc-icon-file-excel-2-fill',
			color: '#67C23A'
		},
		ppt: {
			icon: 'sc-icon-file-ppt-2-fill',
			color: '#F56C6C'
		},
		pptx: {
			icon: 'sc-icon-file-ppt-2-fill',
			color: '#F56C6C'
		}
	}
}
