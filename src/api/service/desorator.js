function isObject(value) {
	return Object.prototype.toString.call(value) === "[object Object]";
}

export function Permission(value) {
	return function (target, key, descriptor) {
		if (!target.permission) {
			target.permission = {};
		}

		setTimeout(() => {
			target.permission[key] = (
				(target.namespace ? target.namespace + "/" : "") + value
			).replace(/\//g, ":");
		}, 0);

		return descriptor;
	};
}

export function Service(value) {
	return function (target) {
		// 命名
		if (typeof value == "string") {
			target.prototype.namespace = value;
		}

		// 复杂项
		if (isObject(value)) {
			const { proxy, namespace, url, mock } = value;

			target.prototype.namespace = namespace;
			target.prototype.mock = mock;

			if (proxy) {
				target.prototype.proxy = proxy;
				target.prototype.url = url
			}
		}
	};
}
