import http from "@/utils/request"

export default class BaseService {
	constructor() {
		const crud = {
			page: "page",
			list: "list",
			info: "info",
			add: "add",
			delete: "delete",
			update: "update"
		};
		if (!this.permission) this.permission = {};

		for (const i in crud) {
			if (this.namespace) {
				this.permission[i] = this.namespace.replace(/\//g, ":") + ":" + crud[i];
			} else {
				this.permission[i] = crud[i];
			}
		}
	}

	request(options = {}) {
		if (!options.params) options.params = {};

		let ns = "";
		if (this.proxy) {
			ns = this.proxy;
		}
		// 拼接前缀
		if (this.namespace) {
			ns += "/" + this.namespace;
		}

		// 处理 http
		if (options.url.indexOf("http") !== 0) {
			options.url = ns + options.url;
		}

		return http.request(options);
	}

	list(params) {
		return this.request({
			url: "/list",
			method: "POST",
			data: {
				...params
			}
		});
	}

	page(params) {
		return this.request({
			url: "/page",
			method: "POST",
			data: {
				...params
			}
		});
	}

	info(params) {
		return this.request({
			url: "/info",
			params: {
				...params
			}
		});
	}

	update(params) {
		return this.request({
			url: "/update",
			method: "POST",
			data: {
				...params
			}
		});
	}

	delete(params) {
		return this.request({
			url: "/delete",
			method: "POST",
			data: {
				...params
			}
		});
	}

	add(params) {
		return this.request({
			url: "/add",
			method: "POST",
			data: {
				...params
			}
		});
	}
}
