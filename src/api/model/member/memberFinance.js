import { BaseService, Service } from "@/api/service";

@Service({
	namespace: "member/memberFinance",
})
class MemberFinance extends BaseService {

}

export default new MemberFinance();
