import { BaseService, Service, Permission } from "@/api/service";

@Service({
	namespace: "member",
})
class Brand extends BaseService {
	@Permission("move")
	move(data) {
		return this.request({
			url: "/move",
			method: "POST",
			data
		});
	}

	@Permission("setStatus")
	setStatus(data) {
		return this.request({
			url: "/setStatus",
			method: "POST",
			data
		});
	}

	@Permission("reSetPwd")
	reSetPwd(data) {
		return this.request({
			url: "/reSetPwd",
			method: "POST",
			data
		});
	}
}

export default new Brand();
