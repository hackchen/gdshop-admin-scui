import { BaseService, Service } from "@/api/service";

@Service({
	namespace: "gdshop/attachmentGroup",
})
class AttachmentGroup extends BaseService {}

export default new AttachmentGroup();
