import { BaseService, Service } from "@/api/service";

@Service({
	namespace: "gdshop/role",
})
class GdshopRole extends BaseService {}

export default new GdshopRole();
