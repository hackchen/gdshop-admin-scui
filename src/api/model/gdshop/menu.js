import { BaseService, Service } from "@/api/service";

@Service({
	namespace: "gdshop/menu",
})
class GdshopMenu extends BaseService {}

export default new GdshopMenu();
