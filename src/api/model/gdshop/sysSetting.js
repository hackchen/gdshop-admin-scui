import { BaseService, Service } from "@/api/service";

@Service({
	namespace: "gdshop/sysSetting",
})
class SysSetting extends BaseService {}

export default new SysSetting();
