import { BaseService, Service,Permission } from "@/api/service";

@Service({
	namespace: "gdshop/common",
})
class GdshopCommon extends BaseService {
	@Permission("areaTree")
	areaTree(params) {
		return this.request({
			url: "/areaTree",
			params: {
				...params
			}
		});
	}

	@Permission("areaList")
	areaList(params) {
		return this.request({
			url: "/areaList",
			params: {
				...params
			}
		});
	}

	@Permission("areaLevelList")
	areaLevelList(params) {
		return this.request({
			url: "/areaLevelList",
			params: {
				...params
			}
		});
	}
}

export default new GdshopCommon();
