import { BaseService, Service, Permission } from "@/api/service";

@Service({
	namespace: "gdshop/attachment",
})
class Attachment extends BaseService {
	@Permission('upload')
	upload(data,config = {}) {
		return this.request({
			url: "/upload",
			method: "POST",
			data: data,
			...config
		});
	}
	@Permission('getBaseUrl')
	getBaseUrl(params) {
		return this.request({
			url: "/get_base_url",
			method: "GET",
			params: params
		});
	}
	@Permission('move')
	move(params) {
		return this.request({
			url: "/move",
			method: "POST",
			data: {
				...params
			}
		});
	}
	@Permission('remotePhoto')
	remotePhoto(params) {
		return this.request({
			url: "/remote_photo",
			method: "POST",
			data: {
				...params
			}
		});
	}

	@Permission('checkMd5')
	checkMd5(params) {
		return this.request({
			url: "/upload_check",
			method: "GET",
			params: params
		});
	}

	@Permission('UploadChunk')
	uploadChunk(data,config = {}) {
		return this.request({
			url: "/upload_chunk",
			method: "POST",
			data: data,
			...config
		});
	}

	@Permission('mergeChunk')
	mergeChunk(data,config = {}) {
		return this.request({
			url: "/merge_chunk",
			method: "POST",
			data: data,
			...config
		});
	}
}

export default new Attachment();
