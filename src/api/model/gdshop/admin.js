import { BaseService, Service, Permission } from "@/api/service";

@Service({
	namespace: "gdshop/admin",
})
class SysAdmin extends BaseService {
	@Permission("move")
	move(data) {
		return this.request({
			url: "/move",
			method: "POST",
			data
		});
	}

	person(data) {
		return this.request({
			url: "/person",
			method: "GET",
			data
		});
	}

	@Permission("resetPass")
	resetPass(data) {
		return this.request({
			url: "/reset_pass",
			method: "POST",
			data
		});
	}

	@Permission("changePass")
	changePass(data) {
		return this.request({
			url: "/change_pass",
			method: "POST",
			data
		});
	}

	permMenu(data) {
		return this.request({
			url: "/permmenu",
			method: "GET",
			data
		});
	}

	/**
	 * 用户信息修改
	 *
	 * @param {*} params
	 * @returns
	 * @memberof CommonService
	 */
	userUpdate(params) {
		return this.request({
			url: "/personUpdate",
			method: "POST",
			data: {
				...params
			}
		});
	}

	/**
	 * 用户退出
	 */
	userLogout() {
		return this.request({
			url: "/logout",
			method: "POST"
		});
	}
}

export default new SysAdmin();
