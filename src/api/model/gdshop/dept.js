import { BaseService, Service, Permission } from "@/api/service";

@Service({
	namespace: "gdshop/department",
})
class GdshopDepartment extends BaseService {
	@Permission("order")
	order(data) {
		return this.request({
			url: "/order",
			method: "POST",
			data
		});
	}
}

export default new GdshopDepartment();
