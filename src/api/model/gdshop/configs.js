import { BaseService, Service } from "@/api/service";

@Service({
	namespace: "gdshop/configs",
})
class GdshopConfigs extends BaseService {}

export default new GdshopConfigs();
