import { BaseService, Service, Permission } from "@/api/service";

@Service({
	namespace: "cms/links",
})
class Links extends BaseService {
	@Permission("move")
	move(data) {
		return this.request({
			url: "/move",
			method: "POST",
			data
		});
	}
}

export default new Links();
