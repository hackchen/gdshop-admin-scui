import { BaseService, Service, Permission } from "@/api/service";

@Service({
	namespace: "cms/slide",
})
class Slide extends BaseService {
	@Permission("move")
	move(data) {
		return this.request({
			url: "/move",
			method: "POST",
			data
		});
	}
}

export default new Slide();
