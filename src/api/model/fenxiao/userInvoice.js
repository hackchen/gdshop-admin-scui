import { BaseService, Service } from "@/api/service";

@Service({
	namespace: "fenxiao/userInvoice",
})
class UserInvoice extends BaseService {
}

export default new UserInvoice();
