import { BaseService, Service } from "@/api/service";

@Service({
	namespace: "fenxiao/order",
})
class Order extends BaseService {
}

export default new Order();
