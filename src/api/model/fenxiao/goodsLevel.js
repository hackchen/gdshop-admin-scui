import { BaseService, Service } from "@/api/service";

@Service({
	namespace: "fenxiao/goodsLevel",
})
class GoodsLevel extends BaseService {
}

export default new GoodsLevel();
