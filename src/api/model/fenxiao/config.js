import { BaseService, Service, Permission } from "@/api/service";

@Service({
	namespace: "fenxiao/config",
})
class Config extends BaseService {
	@Permission("order")
	order(data) {
		return this.request({
			url: "/order",
			method: "POST",
			data
		});
	}

	@Permission("surveys")
	surveys(params) {
		return this.request({
			url: "/surveys",
			params: {
				...params
			}
		});
	}
}

export default new Config();
