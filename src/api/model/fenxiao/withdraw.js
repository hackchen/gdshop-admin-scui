import { BaseService, Service, Permission } from "@/api/service";

@Service({
	namespace: "fenxiao/withdraw",
})
class Withdraw extends BaseService {
	@Permission("changeHandle")
	changeHandle(data = {}) {
		return this.request({
			url: "/change_handle",
			method: "POST",
			data
		});
	}
}

export default new Withdraw();
