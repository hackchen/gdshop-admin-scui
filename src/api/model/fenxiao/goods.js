import { BaseService, Service, Permission } from "@/api/service";

@Service({
	namespace: "fenxiao/goods",
})
class Goods extends BaseService {
	@Permission("changeStatus")
	changeStatus(data = {}) {
		return this.request({
			url: "/change_status",
			method: "POST",
			data
		});
	}
}

export default new Goods();
