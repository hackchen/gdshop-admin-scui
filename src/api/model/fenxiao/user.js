import { BaseService, Service, Permission } from "@/api/service";

@Service({
	namespace: "fenxiao/user",
})
class User extends BaseService {
	@Permission("setStatus")
	setStatus(data = {}) {
		return this.request({
			url: "/setStatus",
			method: "POST",
			data
		});
	}
}

export default new User();
