import { BaseService, Service, Permission } from "@/api/service";

@Service({
	namespace: "shop/goodsActivityGoodsList",
})
class GoodsActivityGoodsList extends BaseService {

	@Permission("delGoods")
	delGoods(params) {
		return this.request({
			url: "/del_goods",
			method: "POST",
			data: {
				...params
			}
		});
	}
}

export default new GoodsActivityGoodsList();
