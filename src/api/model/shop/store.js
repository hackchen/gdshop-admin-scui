import { BaseService, Service, Permission } from "@/api/service";

@Service({
	namespace: "shop/store",
})
class Store extends BaseService {
	@Permission("order")
	order(data) {
		return this.request({
			url: "/order",
			method: "POST",
			data
		});
	}

	@Permission("move")
	move(data) {
		return this.request({
			url: "/move",
			method: "POST",
			data
		});
	}
}

export default new Store();
