import { BaseService, Service } from "@/api/service";

@Service({
	namespace: "shop/config",
})
class Config extends BaseService {

}

export default new Config();
