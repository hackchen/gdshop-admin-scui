import { BaseService, Service, Permission } from "@/api/service";

@Service({
	namespace: "shop/goodsActivity",
})
class TuanActivity extends BaseService {
	activityType = [6];

	@Permission("setStatus")
	setStatus(data) {
		return this.request({
			url: "/setStatus",
			method: "POST",
			data
		});
	}

	@Permission("getRule")
	getRule(params) {
		params.activity_type = 'tuan'
		return this.request({
			url: "/getRule",
			params: {
				...params
			}
		});
	}

	@Permission("getExcludeGoodsSelects")
	getExcludeGoodsSelects(params) {
		return this.request({
			url: "/getExcludeGoodsSelects",
			params: {
				...params
			}
		});
	}

	@Permission("setRule")
	setRule(data) {
		data.activity_type = 'tuan'
		return this.request({
			url: "/setRule",
			method: "POST",
			data
		});
	}

	@Permission("list")
	list(params) {
		if (params.activity_type) {
			params.activity_type = [params.activity_type];
		}
		else {
			params.activity_type = this.activityType;
		}
		return this.request({
			url: "/list",
			method: "POST",
			data: {
				...params
			}
		});
	}

	@Permission("page")
	page(params) {
		if (params.activity_type) {
			params.activity_type = [params.activity_type];
		}
		else {
			params.activity_type = this.activityType;
		}
		return this.request({
			url: "/page",
			method: "POST",
			data: {
				...params
			}
		});
	}
}

export default new TuanActivity();
