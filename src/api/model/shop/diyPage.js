import { BaseService, Service, Permission } from "@/api/service";

@Service({
	namespace: "shop/diyPage",
})
class DiyPage extends BaseService {
	@Permission("setActivated")
	setActivated(data) {
		return this.request({
			url: "/set_activated",
			method: "POST",
			data
		});
	}
}

export default new DiyPage();
