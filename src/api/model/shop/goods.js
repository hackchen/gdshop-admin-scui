import { BaseService, Service, Permission } from "@/api/service";

@Service({
	namespace: "shop/goods",
})
class Goods extends BaseService {
	@Permission("order")
	order(data) {
		return this.request({
			url: "/order",
			method: "POST",
			data
		});
	}

	@Permission("move")
	move(data) {
		return this.request({
			url: "/move",
			method: "POST",
			data
		});
	}
	@Permission("get_form_all")
	get_form_all(data) {
		return this.request({
			url: "/get_form_all",
			method: "GET",
			params: {
				...data
			}
		});
	}
	@Permission("get_form_price")
	get_form_price(data) {
		return this.request({
			url: "/get_form_price",
			method: "GET",
			params: {
				...data
			}
		});
	}
	@Permission("get_form_stock")
	get_form_stock(data) {
		return this.request({
			url: "/get_form_stock",
			method: "GET",
			params: {
				...data
			}
		});
	}
	@Permission("set_form_price")
	set_form_price(data) {
		return this.request({
			url: "/set_form_price",
			method: "POST",
			data
		});
	}
	@Permission("set_form_stock")
	set_form_stock(data) {
		return this.request({
			url: "/set_form_stock",
			method: "POST",
			data
		});
	}

	@Permission("set_admin_disable")
	set_admin_disable(data) {
		return this.request({
			url: "/set_admin_disable",
			method: "POST",
			data
		});
	}

	@Permission("set_status")
	set_status(data) {
		return this.request({
			url: "/set_status",
			method: "POST",
			data
		});
	}

	@Permission("get_diy_goods_data")
	get_diy_goods_data(data) {
		return this.request({
			url: "/get_diy_goods_data",
			method: "GET",
			params: {
				...data
			}
		});
	}

	@Permission("get_activity_option")
	get_activity_option(data) {
		return this.request({
			url: "/get_activity_option",
			method: "GET",
			params: {
				...data
			}
		});
	}

	@Permission("set_activity_option")
	set_activity_option(data) {
		return this.request({
			url: "/set_activity_option",
			method: "POST",
			data
		});
	}
}

export default new Goods();
