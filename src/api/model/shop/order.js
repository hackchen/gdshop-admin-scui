import { BaseService, Service, Permission } from "@/api/service";

@Service({
	namespace: "shop/order",
})
class Order extends BaseService {
	@Permission("order")
	order(data) {
		return this.request({
			url: "/order",
			method: "POST",
			data
		});
	}

	@Permission("move")
	move(data) {
		return this.request({
			url: "/move",
			method: "POST",
			data
		});
	}

	@Permission("changeRemark")
	changeRemark(data) {
		return this.request({
			url: "/change_remark",
			method: "POST",
			data
		});
	}

	@Permission("changeStatus")
	changeStatus(data) {
		return this.request({
			url: "/change_status",
			method: "POST",
			data
		});
	}

	@Permission("setPriceChange")
	setPriceChange(data) {
		return this.request({
			url: "/set_price_change",
			method: "POST",
			data
		});
	}

	@Permission("getAddressInfo")
	getAddressInfo(params) {
		return this.request({
			url: "/get_address_info",
			params: {
				...params
			}
		});
	}

	@Permission("getExpressInfo")
	getExpressInfo(params) {
		return this.request({
			url: "/get_express_info",
			params: {
				...params
			}
		});
	}

	@Permission("getExpressInfoList")
	getExpressInfoList(params) {
		return this.request({
			url: "/get_express_info_list",
			params: {
				...params
			}
		});
	}

	@Permission("getListTypes")
	getListTypes(params) {
		return this.request({
			url: "/get_list_types",
			params: {
				...params
			}
		});
	}

	@Permission("discountInfo")
	discountInfo(params) {
		return this.request({
			url: "/discount_info",
			params: {
				...params
			}
		});
	}
}

export default new Order();
