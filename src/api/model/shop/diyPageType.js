import { BaseService, Service, Permission } from "@/api/service";

@Service({
	namespace: "shop/diyPageType",
})
class DiyPageType extends BaseService {
	@Permission("order")
	order(data) {
		return this.request({
			url: "/order",
			method: "POST",
			data
		});
	}
}

export default new DiyPageType();
