import { BaseService, Service, Permission } from "@/api/service";

@Service({
	namespace: "shop/coupon",
})
class Coupon extends BaseService {
	@Permission("order")
	order(data) {
		return this.request({
			url: "/order",
			method: "POST",
			data
		});
	}

	@Permission("changeStatus")
	changeStatus(data) {
		return this.request({
			url: "/change_status",
			method: "POST",
			data
		});
	}

	@Permission("getRemark")
	getRemark(params) {
		return this.request({
			url: "/get_remark",
			params: {
				...params
			}
		});
	}

	@Permission("setRemark")
	setRemark(data) {
		return this.request({
			url: "/set_remark",
			method: "POST",
			data
		});
	}

	@Permission("ending")
	ending(data) {
		return this.request({
			url: "/ending",
			method: "POST",
			data
		});
	}
}

export default new Coupon();
