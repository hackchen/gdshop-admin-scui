import { BaseService, Service } from "@/api/service";

@Service({
	namespace: "shop/express",
})
class Express extends BaseService {}

export default new Express();
