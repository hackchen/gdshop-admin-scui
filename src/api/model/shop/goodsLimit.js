import { BaseService, Service, Permission } from "@/api/service";

@Service({
	namespace: "shop/goodsLimit",
})
class GoodsLimit extends BaseService {
	@Permission("setStatus")
	setStatus(data) {
		return this.request({
			url: "/setStatus",
			method: "POST",
			data
		});
	}

	@Permission("updateLimitRules")
	updateLimitRules(data) {
		return this.request({
			url: "/updateLimitRules",
			method: "POST",
			data
		});
	}

	@Permission("getLimitRules")
	getLimitRules(params) {
		return this.request({
			url: "/getLimitRules",
			params: {
				...params
			}
		});
	}
}

export default new GoodsLimit();
