import { BaseService, Service, Permission } from "@/api/service";

@Service({
	namespace: "shop/goodsActivity",
})
class GoodsActivity extends BaseService {
	activityType = [1,2];

	@Permission("setStatus")
	setStatus(data) {
		if (data.activity_type) {
			data.activity_type = [data.activity_type];
		}
		else {
			data.activity_type = this.activityType;
		}
		return this.request({
			url: "/setStatus",
			method: "POST",
			data
		});
	}

	@Permission("getRule")
	getRule(params) {
		if (params.activity_type) {
			params.activity_type = [params.activity_type];
		}
		else {
			params.activity_type = this.activityType;
		}
		return this.request({
			url: "/getRule",
			params: {
				...params
			}
		});
	}

	@Permission("getExcludeGoodsSelects")
	getExcludeGoodsSelects(params) {
		return this.request({
			url: "/getExcludeGoodsSelects",
			params: {
				...params
			}
		});
	}

	@Permission("setRule")
	setRule(data) {
		return this.request({
			url: "/setRule",
			method: "POST",
			data
		});
	}

	@Permission("list")
	list(params) {
		if (params.activity_type) {
			params.activity_type = [params.activity_type];
		}
		else {
			params.activity_type = this.activityType;
		}
		return this.request({
			url: "/list",
			method: "POST",
			data: {
				...params
			}
		});
	}

	@Permission("page")
	page(params) {
		if (params.activity_type) {
			params.activity_type = [params.activity_type];
		}
		else {
			params.activity_type = this.activityType;
		}
		return this.request({
			url: "/page",
			method: "POST",
			data: {
				...params
			}
		});
	}
}

export default new GoodsActivity();
