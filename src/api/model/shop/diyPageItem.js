import { BaseService, Service, Permission } from "@/api/service";

@Service({
	namespace: "shop/diyPageItem",
})
class DiyPageItem extends BaseService {
	@Permission("order")
	order(data) {
		return this.request({
			url: "/order",
			method: "POST",
			data
		});
	}
}

export default new DiyPageItem();
