import { BaseService, Service, Permission } from "@/api/service";

@Service({
	namespace: "shop/orderRefund",
})
class OrderRefund extends BaseService {
	@Permission("order")
	order(data) {
		return this.request({
			url: "/order",
			method: "POST",
			data
		});
	}

	@Permission("getListTypes")
	getListTypes(params) {
		return this.request({
			url: "/get_list_types",
			params: {
				...params
			}
		});
	}

	@Permission("getHandle")
	getHandle(params) {
		return this.request({
			url: "/getHandle",
			params: {
				...params
			}
		});
	}

	@Permission("refund")
	refund(params) {
		return this.request({
			url: "/refund",
			params: {
				...params
			}
		});
	}

	@Permission("submitHandle")
	submitHandle(data) {
		return this.request({
			url: "/submitHandle",
			method: "POST",
			data
		});
	}
}

export default new OrderRefund();
