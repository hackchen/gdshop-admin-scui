// 下面代码自动导入自定义 icon
const files = require.context('./vue/', false, /\.vue$/)
const modules = {}
files.keys().forEach((key) => {
	modules[key.replace(/(\.\/|\.vue)/g, '')] = files(key).default
})

export default modules
